﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assesmenttask02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Please enter the name of the month you were born in");
            var month = Console.ReadLine();

            Console.WriteLine($"Please enter the name of the day you were born on");
            var day = Console.ReadLine();

            Console.WriteLine($"You were born on the {day}th of {month}");
        }
    }
}
