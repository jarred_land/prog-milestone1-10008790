﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask36
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This code will use a for loop and if statement to monitor the state of the counter");

            var counter = 3;
            var index = 0;

            for (index = 0; index < counter; index++)
            {
                if (index != counter)
                {
                    Console.WriteLine("The index is currently not equal to the counter");
                }
                else
                {
                    Console.WriteLine("The index is now equal to the counter");
                }
                
            }
            Console.WriteLine("This index is now equal to the counter and this loop is complete");
        }
    }
}
