﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the year you wish to check");
            var year = Console.ReadLine();
            int checkyear;
            checkyear = int.Parse(year);

            bool result = DateTime.IsLeapYear(checkyear);

            if (result.Equals(true))
            {
                Console.WriteLine($"Yes, {year} was a leap year");
            }
            else
            {
                Console.WriteLine($"No, {year} was not a leap year.");
            }

        }
    }
}
