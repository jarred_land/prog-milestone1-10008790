﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask4
{
    class Program
    {
        static void Main(string[] args)
        {
            const Double Celsius = 1;
            const Double Fahrenheit = 33.8;
            int number = (0);

            Console.WriteLine($"Please type c2f  to convert Celsius to Fahrenheit, or type f2c to convert from Farenheit to Celsius");

            var input = Console.ReadLine();

            if (input == "c2f")
            {
                Console.WriteLine("Enter your number in Celsius");
                number = int.Parse(Console.ReadLine());
                Console.WriteLine($"{number * 9 / 5 + 32}");

            }

            else
            {
                Console.WriteLine("Enter your number in Fahrenheit");
                number = int.Parse(Console.ReadLine());
                Console.WriteLine($"{(number - 32) * 5 / 9}");

            }
            return;
        }
    }
}
