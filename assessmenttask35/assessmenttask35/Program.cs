﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The purpose of this program is to recieve 3 number from you and use those numbers to ask 5 different math equations");
            Console.WriteLine("Please enter a single-digit number");

            var number1 = 0;
            var userinput1 = Console.ReadLine();
            number1 = int.Parse(userinput1);

            Console.WriteLine("Please enter another single-digit number");

            var number2 = 0;
            var userinput2 = Console.ReadLine();
            number2 = int.Parse(userinput2);

            Console.WriteLine("Please enter one more single-digit number");

            var number3 = 0;
            var userinput3 = Console.ReadLine();
            number3 = int.Parse(userinput3);

            Console.WriteLine("Thank you. We will now go through the equations");

            //Equation 1

            Console.WriteLine($"What is {number1} + {number2}?");

            var answer1 = Console.ReadLine();
            int useranswer1;
            useranswer1 = int.Parse(answer1);

            var check1 = number1 + number2;
            check1 = int.Parse(answer1);

            if (useranswer1 == check1)
            {
                Console.WriteLine($"You are correct, the answer is {check1}");
            }
            else
            {
                Console.WriteLine("You are incorrect");
            }

            //Equation 2

            Console.WriteLine($"What is {number2} * {number3}?");

            var answer2 = Console.ReadLine();
            int useranswer2;
            useranswer2 = int.Parse(answer2);

            var check2 = number2 * number3;
            check1 = int.Parse(answer1);

            if (useranswer2 == check2)
            {
                Console.WriteLine($"You are correct, the answer is {check2}");
            }
            else
            {
                Console.WriteLine("You are incorrect");
            }

            //Equation 3

            Console.WriteLine($"What is {number3} + {number1}?");

            var answer3 = Console.ReadLine();
            int useranswer3;
            useranswer3 = int.Parse(answer3);

            var check3 = number3 + number1;
            check1 = int.Parse(answer3);

            if (useranswer3 == check3)
            {
                Console.WriteLine($"You are correct, the answer is {check3}");
            }
            else
            {
                Console.WriteLine("You are incorrect");
            }

            //Equation 4

            Console.WriteLine($"What is {number1} - {number3}?");

            var answer4 = Console.ReadLine();
            int useranswer4;
            useranswer4 = int.Parse(answer4);

            var check4 = number1 - number3;
            check4 = int.Parse(answer4);

            if (useranswer4 == check4)
            {
                Console.WriteLine($"You are correct, the answer is {check4}");
            }
            else
            {
                Console.WriteLine("You are incorrect");
            }

            //Equation 5

            Console.WriteLine($"What is {number3} - {number2}?");

            var answer5 = Console.ReadLine();
            int useranswer5;
            useranswer5 = int.Parse(answer5);

            var check5 = number3 - number2;
            check5 = int.Parse(answer5);

            if (useranswer5 == check5)
            {
                Console.WriteLine($"You are correct, the answer is {check5}");
            }
            else
            {
                Console.WriteLine("You are incorrect");
            }
        }
    }
}
