﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask26
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new String[5] { "Red", "Blue", "Yellow", "Green", "Pink", };

            Console.WriteLine("The purpose of this code is the display an array of colours in descending order");

            Array.Sort(colours);
            Array.Reverse(colours);

            Console.WriteLine(string.Join(",", colours));

        }
    }
}
