﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask33
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 28;
            
            Console.WriteLine("Please enter the number of students you have");
            var input = Console.ReadLine();
            double answer = int.Parse(input);

            double total = answer / a;

            Console.WriteLine($"You are able to have {total} tutorial groups with {a} students in each");


        }
    }
}
