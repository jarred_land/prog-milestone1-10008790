﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter military time");

            var input = int.Parse(Console.ReadLine());
            var a = input - 1200;
            var b = a / 100;
            var c = input / 100;

            if (input > 1300)
            {
                Console.WriteLine($"The time is {b}pm");
            }
            else
            {
                Console.WriteLine($"The time is {c}am");
            }
        }
    }
}
