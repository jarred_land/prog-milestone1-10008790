﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask13
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the first price");

            var userinput1 = Console.ReadLine();
            double number1;
            number1 = double.Parse(userinput1);

            Console.WriteLine("Please enter the second price");

            var userinput2 = Console.ReadLine();
            double number2;
            number2 = double.Parse(userinput2);

            Console.WriteLine("Please enter the third price");

            var userinput3 = Console.ReadLine();
            double number3;
            number3 = double.Parse(userinput3);

            double answer = number1 + number2 + number3;
            double gst = 1.15;

            Console.WriteLine($"With an added GST of 15%, the total of the prices you entered is ${answer * gst}");
            
            
            
        }
    }
}
