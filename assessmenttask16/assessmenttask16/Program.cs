﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word and this program will count the number of letters in that word");
            var word = Console.ReadLine();

            int numberOfLetters = 0;
            foreach (char letter in word)
            {
                numberOfLetters++;
            }

            Console.WriteLine($"There are {numberOfLetters} letters in {word}");
        }
    }
}
