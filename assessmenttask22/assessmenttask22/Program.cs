﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask22
{
    class Program
    {
        static void Main(string[] args)
        {
            var dic = new Dictionary<string, string>();
            var fruitlist = new List<string> { };
            var veglist = new List<string> { };

            dic.Add("Apple", "Fruit");
            dic.Add("Carrot", "Vegetable");
            dic.Add("Banana", "Fruit");
            dic.Add("Beetroot", "Vegetable");
            dic.Add("Orange", "Fruit");
            dic.Add("Potato", "Vegetable");
            dic.Add("Onion", "Vegetable");

            foreach (var x in dic)
            {
                if (x.Value == "Fruit")
                {
                    fruitlist.Add(x.Key);
                }
                else
                {
                    veglist.Add(x.Key);
                }
            }

                Console.WriteLine($"There are {fruitlist.Count} fruits in this list");
                Console.WriteLine($"There are {veglist.Count} vegetables in this list");
                Console.WriteLine($"{string.Join(",", fruitlist)} are the fruits in this list");
            
        }
    }
}
