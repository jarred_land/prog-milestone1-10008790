﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessmenttask38
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter the number you wish to see the 1-12 division table of: ");

            var number = Console.ReadLine();
            int userinput = int.Parse(number);

            if (userinput == 0)
                return;
            for (int i = 1; i <= 12; i++)
                Console.WriteLine("{0} / {1} = {2}", i, userinput, i / userinput);
        }
    }
}
